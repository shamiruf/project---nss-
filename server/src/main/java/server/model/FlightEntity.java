package server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.CreationTimestamp;
import server.model.serializer.PassengerSerializer;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "flight", schema = "airbook", catalog = "ear2018zs_22")
public class FlightEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Integer id;
    @NotNull
    private Integer planeid;
    @NotNull
    private Integer airlineid;
    @NotNull
    private Timestamp departmentdate;
    @NotNull
    private Timestamp arrilvalsdate;
    @NotNull
    private String department;
    @NotNull
    private String arrival;
    @NotNull
    private Integer distance;
    @NotNull
    private Integer terminalid;

    private AirlineEntity airlineByAirlineid;
    private TerminalEntity terminalByTerminalid;
    private List<TicketEntity> ticketsById;

    @JsonSerialize(using = PassengerSerializer.class)
    private List<PassengerEntity> passengers;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "planeid")
    public Integer getPlaneid() {
        return planeid;
    }

    public void setPlaneid(Integer planeid) {
        this.planeid = planeid;
    }

    @Basic
    @Column(name = "airlineid")
    public Integer getAirlineid() {
        return airlineid;
    }

    public void setAirlineid(Integer airlineid) {
        this.airlineid = airlineid;
    }

    @Basic
    @CreationTimestamp
    @Column(name = "departmentdate")
    public Timestamp getDepartmentdate() {
        return departmentdate;
    }

    public void setDepartmentdate(Timestamp departmentdate) {
        this.departmentdate = departmentdate;
    }

    @Basic
    @CreationTimestamp
    @Column(name = "arrilvalsdate")
    public Timestamp getArrilvalsdate() {
        return arrilvalsdate;
    }

    public void setArrilvalsdate(Timestamp arrilvalsdate) {
        this.arrilvalsdate = arrilvalsdate;
    }

    @Basic
    @Column(name = "department")
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Basic
    @Column(name = "arrival")
    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    @Basic
    @Column(name = "distance")
    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "terminalid")
    public Integer getTerminalid() {
        return terminalid;
    }

    public void setTerminalid(Integer terminalid) {
        this.terminalid = terminalid;
    }

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "airlineid", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public AirlineEntity getAirlineByAirlineid() {
        return airlineByAirlineid;
    }

    public void setAirlineByAirlineid(AirlineEntity airlineByAirlineid) {
        this.airlineByAirlineid = airlineByAirlineid;
    }

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "terminalid", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public TerminalEntity getTerminalByTerminalid() {
        return terminalByTerminalid;
    }

    public void setTerminalByTerminalid(TerminalEntity terminalByTerminalid) {
        this.terminalByTerminalid = terminalByTerminalid;
    }


    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "flightByFlightid")
    public List<TicketEntity> getTicketsById() {
        return ticketsById;
    }

    public void setTicketsById(List<TicketEntity> ticketsById) {
        this.ticketsById = ticketsById;
    }

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ticket", catalog = "airbook",
            joinColumns = {@JoinColumn(name = "flightid", nullable = false, updatable = false)},
            inverseJoinColumns = { @JoinColumn(name = "passengerid", nullable = false, updatable = false)})
    public List<PassengerEntity> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengerEntity> passengers) {
        this.passengers = passengers;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, planeid, airlineid, departmentdate, arrilvalsdate, department, arrival, distance, terminalid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightEntity that = (FlightEntity) o;
        return id == that.id &&
                planeid == that.planeid &&
                airlineid == that.airlineid &&
                distance == that.distance &&
                terminalid == that.terminalid &&
                Objects.equals(departmentdate, that.departmentdate) &&
                Objects.equals(arrilvalsdate, that.arrilvalsdate) &&
                Objects.equals(department, that.department) &&
                Objects.equals(arrival, that.arrival);
    }
}