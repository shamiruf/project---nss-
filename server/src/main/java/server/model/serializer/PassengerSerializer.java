package server.model.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import server.model.PassengerEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PassengerSerializer extends StdSerializer<List<PassengerEntity>> {
    public PassengerSerializer() {
        this(null);
    }

    private PassengerSerializer(final Class<List<PassengerEntity>> t) {
        super(t);
    }

    @Override
    public void serialize(List<PassengerEntity> passengers, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
        List<PassengerEntity> ids = new ArrayList<>();
        for (PassengerEntity passenger : passengers) {
            PassengerEntity pas = passenger;
            pas.setFlights(null);
            pas.setTicketsById(null);
            ids.add(pas);
        }
        generator.writeObject(ids);
    }
}
