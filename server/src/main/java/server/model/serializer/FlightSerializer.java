package server.model.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import server.model.FlightEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FlightSerializer extends StdSerializer<List<FlightEntity>> {
    public FlightSerializer() {
        this(null);
    }

    private FlightSerializer(final Class<List<FlightEntity>> t) {
        super(t);
    }

    @Override
    public void serialize(List<FlightEntity> flights, JsonGenerator generator, SerializerProvider provider) throws IOException, JsonProcessingException {
        List<FlightEntity> ids = new ArrayList<>();
        for (FlightEntity flight : flights) {
            FlightEntity fl = flight;
            fl.setPassengers(null);
            fl.setTicketsById(null);
            ids.add(fl);
        }
        generator.writeObject(ids);
    }
}
