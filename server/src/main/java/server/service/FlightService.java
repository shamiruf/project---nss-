package server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import server.dto.FlightDto;
import server.model.FlightEntity;
import server.repositories.FlightRepository;
import server.repositories.PassengerRepository;
import server.service.Interfaces.FlightServiceInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FlightService implements FlightServiceInterface {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private PassengerRepository passengerRepository;

    @Autowired
    public FlightService(FlightRepository repository) {
        this.flightRepository = repository;
    }

    public FlightService() {
    }

    @Override
    public FlightEntity create(FlightDto flightDto) {
        return flightRepository.saveAndFlush(flightDto.toEntity());
    }

    @Override
    public List<FlightEntity> getAllFlight() {
//        List<FlightEntity> flightEntities = new ArrayList<>();
//        flightEntities = flightRepository.findAll();
        return flightRepository.findAll();
    }

    @Override
    public Optional<FlightEntity> getFlightByID(int id) {
        return flightRepository.findById(id);
    }

    @Override
    public void update(FlightDto flightDto) {
        flightRepository.saveAndFlush(flightDto.toEntity());
    }

    public void updatePlane(Integer id, Integer newPlane){
        FlightEntity passenger = flightRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id.toString()));
        passenger.setPlaneid(newPlane);
        flightRepository.saveAndFlush(passenger);
    }

    @Override
    public void delete(FlightEntity flight) {
        flightRepository.saveAndFlush(flight);
    }

    @Override
    public void deleteById(int id) {
        flightRepository.deleteById(id);
    }
}