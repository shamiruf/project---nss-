$("form").submit(function() {
    let formData = {}
    $(this).serializeArray().forEach(function (value) {
        formData["accept"] = true
        if (value.value === "" || null) {
            formData["accept"] = false
        }
        else {
            formData[value.name] = value.value
        }
    })


    if (formData.accept){
        delete formData.accept
        formData["id"] = Math.random() * (999 - 34) + 34
        formData["airlineid"] = 2
        formData["planeid"] = 3
        formData["terminalid"] = 2
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "flight/create/", false)
        xhr.setRequestHeader("Content-Type", "application/json")
        let data = JSON.stringify(formData)
        xhr.send(data)
        alert(xhr.status + ': ' + xhr.stack);
    }
    else {
        alert("WTF ?! Control your data !!!")
    }
    event.preventDefault()
});

sendGetCheck();

function sendGetCheck() {
    document.getElementById("flights").innerHTML = ""


    let xhr = new XMLHttpRequest();

// 2. configure GET for URL 'flights.json'
    xhr.open('GET', 'flight/getAllFlight', false);

// 3. send request
    xhr.send();

// 4. if response status 200, then error
    if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    } else {
        let flightsResponse = JSON.parse(xhr.response);
        let allFlights = document.createElement('span')
        for (let i in flightsResponse){
            let flightCard = document.createElement('div')
            flightCard.className = "card"
            flightCard.innerHTML = createFlightCard(flightsResponse[i])
            allFlights.appendChild(flightCard)
            allFlights.appendChild(document.createElement('br'))
        }
        document.getElementById("flights").appendChild(allFlights);
    }
}


function createFlightCard(flightobject){
    let flightHeader = "<div class=\"card-header\">\n" +
        "\n" + flightobject.department + " &#x27A1 " + flightobject.arrival +
        "       <a href='#' onclick=deleteFlight(" + flightobject.id + ")><i class=\"material-icons float-right\">delete</i></a> \n" +
        "    </div>"
    console.log(flightobject)
    let flightBody = "<div class=\"card-body\">\n" +
        "        <blockquote class=\"blockquote mb-0\">\n" +
        "<p> departure: " + new Date(flightobject.departmentdate).toLocaleString("cs-CZ") + "</p>" +
        "            <p> arrival: " + new Date(flightobject.arrilvalsdate).toLocaleString("cs-CZ") + "</p>\n" +
        "            <p> distance: " + flightobject.distance + "</p>\n" +
        "    </div>"
    return flightHeader + flightBody;
}

function deleteFlight(flightId) {
    let xhr = new XMLHttpRequest();
    xhr.open('delete', 'flight/delete/' + flightId,false)
    xhr.send()
    sendGetCheck()
}


