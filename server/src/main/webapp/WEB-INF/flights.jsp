<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Manage Flights</title>
    <br>
    <link rel="stylesheet" type="text/css" href="../flightsresources/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div id = "flights"></div>
<hr>
<div class="card">
    <div class="card-header">
        Add Flight
    </div>
    <div class="card-body">
        <form id="addFlightForm">
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputFrom">From</label>
                    <input type="text" name="department" class="form-control" id="inputFrom">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputTo">To</label>
                    <input type="text" name="arrival" class="form-control" id="inputTo">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label for="inputArrival">Arrival</label>
                    <input type="datetime-local" name="arrilvalsdate" class="form-control" id="inputArrival">
                </div>
                <div class="form-group col-md-5">
                    <label for="inputDeparture">Departure</label>
                    <input type="datetime-local" name="departmentdate" class="form-control" id="inputDeparture">
                </div>
                <div class="form-group col-md-2">
                    <label for="inputDistance">Distance</label>
                    <input type="number" name="distance" class="form-control" id="inputDistance">
                </div>
            </div>
            <button type="submit" class="btn btn-primary float-right">Add Flight</button>
        </form>
    </div>
</div>
<br>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="../flightsresources/common.js"></script>
</html>