## Project description
AIS - systém určený k podpoře výrobních, technologických a finančních procesů leteckého podniku. 
To vám umožní řešit problémy spojené s organizací, přípravou a prováděním prací na zajištění letecké dopravy na letišti.

## [Team](Tým)

<table>
       <tr><td>Andrei Zhikulin</td><td>@zhikuand</td></tr>
       <tr><td>Ruf Shamikh</td><td>@shamiruf</td></tr>
       <tr><td>Vasil Merkul</td><td>@merkuvas</td></tr>
</table>